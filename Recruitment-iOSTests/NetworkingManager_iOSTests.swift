//
//  NetworkingManager_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by Jakub Tasiemski on 24/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class NetworkingManager_iOSTests: XCTestCase {

    var networkManager: NetworkingManager!
    var delegate: MockNetworkingManagerDelegate!
        
    override func setUp() {
        super.setUp()
        
        self.networkManager = NetworkingManager()
        self.delegate = MockNetworkingManagerDelegate()
        self.networkManager.delegate = delegate
    }
    
    func testDownloadItems() throws {
        let expectation = self.expectation(description: "Downloading items")
        
        delegate.expectation = expectation
        networkManager.downloadItems()
        
        waitForExpectations(timeout: 3, handler: nil)
    }
    
    func testDownloadSingleItem() throws {
        let expectation = self.expectation(description: "Downloading item")
        networkManager.delegate = delegate
        
        delegate.expectation = expectation
        networkManager.downloadItemWithID("1")
        
        waitForExpectations(timeout: 3, handler: nil)
        XCTAssertNotNil(delegate.downloadedItem)
        XCTAssertEqual(delegate.downloadedItem?.name, "Item1")
        XCTAssertEqual(delegate.downloadedItem?.color, UIColor.green)
        XCTAssertEqual(delegate.downloadedItem?.desc, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a lacus felis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam dignissim facilisis quam et malesuada. Sed imperdiet ipsum ut elit porttitor gravida. Nulla a lectus lorem. Proin sed volutpat risus. Aenean malesuada, nisl id finibus pellentesque, nunc felis pulvinar tellus, in commodo nisl urna et leo. Donec ac ante tortor. Pellentesque consequat tellus nec pellentesque euismod. Mauris euismod, leo auctor aliquet lacinia, dui est pulvinar nulla, fermentum dapibus felis leo nec est. Fusce ultrices, risus at ullamcorper cursus, leo sapien mattis arcu, vitae facilisis massa lacus et libero. Etiam sollicitudin augue porttitor tristique convallis. Aenean imperdiet, tortor dictum tristique ullamcorper, nulla nulla convallis tellus, sit amet luctus purus augue ac leo.")
    }
    
    func testDownloadNotExistingItem() throws {
        let expectation = self.expectation(description: "Downloading item")
        networkManager.delegate = delegate
        
        delegate.expectation = expectation
        networkManager.downloadItemWithID("#$")
        
        waitForExpectations(timeout: 3, handler: nil)
        XCTAssertNil(delegate.downloadedItem)
    }
    
    func testDownloadBrokenItem() throws {
        let expectation = self.expectation(description: "Downloading item")
        networkManager.delegate = delegate
        
        delegate.expectation = expectation
        networkManager.downloadItemWithID("1Broken")
        
        waitForExpectations(timeout: 3, handler: nil)
        XCTAssertNil(delegate.downloadedItem)
    }
}

class MockNetworkingManagerDelegate: NetworkingManagerDelegate {
    var expectation: XCTestExpectation?
    var downloadedItem: ItemDetailsModel?
    
    func downloadedItems(_ items: [ItemPreviewModel]) {
        expectation?.fulfill()
        XCTAssertEqual(items.count, 5)
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?)  {
        expectation?.fulfill()
        downloadedItem = itemDetails
    }
    
    
}
