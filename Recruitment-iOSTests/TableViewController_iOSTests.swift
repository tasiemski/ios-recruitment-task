//
//  TableCiewController_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by Jakub Tasiemski on 24/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class TableViewController_iOSTests: XCTestCase {

    var tableViewController: TableViewController!
        
        override func setUp() {
            super.setUp()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.tableViewController = storyboard.instantiateViewController(withIdentifier: "TableViewControllerID") as? TableViewController
            
            self.tableViewController.loadView()
        }

    func testSingleCell() throws {
        tableViewController.downloadedItems([ItemPreviewModel(id: "1", attributes: ["name":"A","color":"Red","preview":"Abcdef1"])])
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableViewController.tableView.cellForRow(at: indexPath) as? ItemTableViewCell

        XCTAssertNotNil(cell)
        XCTAssertEqual(cell?.textLabel?.text, "A")
        XCTAssertEqual(cell?.backgroundColor, UIColor.red)
        XCTAssertEqual(cell?.detailTextLabel?.text, "Abcdef1")
    }

}
