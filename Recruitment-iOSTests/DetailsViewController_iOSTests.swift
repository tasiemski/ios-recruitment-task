//
//  DetailsViewController_iOSTests.swift
//  Recruitment-iOSTests
//
//  Created by Jakub Tasiemski on 24/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest

@testable import Recruitment_iOS

class DetailsViewController_iOSTests: XCTestCase {

    var detailsViewController: DetailsViewController!
        
        override func setUp() {
            super.setUp()
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.detailsViewController = storyboard.instantiateViewController(withIdentifier: "DetailsViewControllerID") as? DetailsViewController
            
            self.detailsViewController.loadView()
        }

    func testEmoStyleFormatting() throws {
        detailsViewController.item = ItemModel(id: "Id1", attributes: ["name":"Item123","color":"Red"])
        XCTAssertEqual(detailsViewController.title, "ItEm123")
        
        detailsViewController.item = ItemModel(id: "Id2", attributes: ["name":"My beatiful_item","color":"Blue"])
        XCTAssertEqual(detailsViewController.title, "My bEaTiFuL_ItEm")
    }
    
    func testLoadCorrectItem() throws {
        let desc = "Abcdef1"
        let id = "2"
        let color = "Red"
        detailsViewController.item = ItemModel(id: id, attributes: ["name":"Item2","color":color])
        XCTAssertEqual(detailsViewController.textView.text, "Loading...")
        
        detailsViewController.downloadedItemDetails(ItemDetailsModel(id: id, attributes: ["name":"Item2","color":"Red","desc":desc]))

        XCTAssertEqual(detailsViewController.view.backgroundColor, UIColor.red)
        XCTAssertEqual(detailsViewController.textView.text, desc)
    }

}
