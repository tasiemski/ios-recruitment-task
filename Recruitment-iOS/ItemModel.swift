//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

enum ItemType: String, Codable {
    case Items, ItemDetails
}

class ItemModel: Codable {

    let id:String
    let attributes: [String: String]

    var name: String? { return attributes["name"] }

    var color: UIColor {
        get {
            switch attributes["color"] {
            case "Red": return UIColor.red
            case "Green": return UIColor.green
            case "Blue": return UIColor.blue
            case "Yellow": return UIColor.yellow
            case "Purple": return UIColor.purple
            default: return UIColor.black
            }
        }
    }
    
    init(id: String, attributes: [String: String]) {
        self.id = id
        self.attributes = attributes
    }

}
