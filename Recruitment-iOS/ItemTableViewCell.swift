//
//  ItemTableViewCell.swift
//  Recruitment-iOS
//
//  Created by Jakub Tasiemski on 20/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    var itemModel: ItemPreviewModel? {
        didSet {
            self.backgroundColor = itemModel?.color
            self.textLabel?.text = itemModel?.name
            self.detailTextLabel?.text = itemModel?.preview
        }
    }
}
