//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by Jakub Tasiemski on 24/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    
    var item: ItemPreviewModel? {
        didSet {
            self.title.text = item?.name
            self.backgroundColor = item?.color
        }
    }
    
}
