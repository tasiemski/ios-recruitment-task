//
//  ItemPreviewModel.swift
//  Recruitment-iOS
//
//  Created by Jakub Tasiemski on 23/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemPreviewModel: ItemModel {
    var preview: String? { return attributes["preview"] }
}
