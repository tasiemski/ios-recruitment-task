//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, NetworkingManagerDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    var item: ItemModel? {
        didSet {
            self.view.backgroundColor = item?.color
            self.title = emoStyleFormatting(text: item!.name!)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItemWithID(item!.id)
    }

    func downloadedItems(_ items: [ItemPreviewModel]) {
    }

    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
        guard let item = itemDetails else {
            return
        }
        textView.text = item.desc
    }

    private func emoStyleFormatting(text: String) -> String {
        return String(text.enumerated().map { index, letter in
            return index % 2 == 0 ?
                Character(letter.uppercased()) : Character(letter.lowercased())
        })
    }

}
