//
//  JSONParser.swift
//  Route1
//
//  Created by Paweł Sporysz on 11.12.2015.
//  Copyright © 2015 Untitled Kingdom. All rights reserved.
//

import Foundation

class JSONParser {
    
    static func dataFromFilename(_ filename:String) -> Data? {
        guard let filepath = Bundle.main.path(forResource: filename, ofType: "") else { return nil }
        do {
            let stringContent = try String(contentsOfFile: filepath, encoding: String.Encoding.utf8)
            return stringContent.data(using: String.Encoding.utf8) ?? nil
        } catch _ {}
        return nil
    }
    
}
