//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by Jakub Tasiemski on 24/01/2021.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, NetworkingManagerDelegate {
    
    var itemModels:[ItemPreviewModel] = []
    
    override func viewDidLoad() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.itemModels.count > 0 ? (self.itemModels.count/2) + 1 : 0
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (itemModels.count/(section + 1)) >= 2 ? 2 : 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCellID", for: indexPath) as! CollectionViewCell
        cell.item = itemModels[indexPath.section*2+indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if indexPath.section*2+indexPath.row == itemModels.count - 1 && itemModels.count % 2 == 1 {
            return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: 70)
        }
        return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width/2-1, height: 70)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsViewSegue" {
            let senderCell = sender as? CollectionViewCell
            let detailsViewController = segue.destination as? DetailsViewController
            detailsViewController?.item = senderCell?.item
        }
    }

    func downloadedItems(_ items: [ItemPreviewModel]) {
        self.itemModels = items
        self.collectionView.reloadData()
    }

    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
    }

}
