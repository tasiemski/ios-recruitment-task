//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, NetworkingManagerDelegate {
    
    var itemModels:[ItemPreviewModel] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! ItemTableViewCell
        cell.itemModel = itemModels[indexPath.row]
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsViewSegue" {
            let senderCell = sender as? ItemTableViewCell
            let detailsViewController = segue.destination as? DetailsViewController
            detailsViewController?.item = senderCell?.itemModel
        }
    }

    func downloadedItems(_ items: [ItemPreviewModel]) {
        self.itemModels = items
        self.tableView.reloadData()
    }

    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
    }
    
}
