//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

protocol NetworkingManagerDelegate: class {
    
    func downloadedItems(_ items:[ItemPreviewModel])
    func downloadedItemDetails(_ itemDetails:ItemDetailsModel?)
    
}

class Items: Codable {
    let data: [ItemPreviewModel]
}

class Item: Codable {
    let data: ItemDetailsModel
}

class NetworkingManager: NSObject {

    static var sharedManager = NetworkingManager()
    
    weak var delegate:NetworkingManagerDelegate?
    
    func downloadItems() {
        request(filename: "Items.json") { downloadedData in
            guard let data = downloadedData else {
                // log
                self.delegate?.downloadedItems([])
                return
            }
            do {
                let items: Items = try JSONDecoder().decode(Items.self, from: data)
                self.delegate?.downloadedItems(items.data)
            } catch {
                // log (...)
                self.delegate?.downloadedItems([])
            }
        }
    }
    
    func downloadItemWithID(_ id:String) {
        
        let filename = "Item\(id).json"
        request(filename: filename) { downloadedData in
            guard let data = downloadedData else {
                // log
                self.delegate?.downloadedItemDetails(nil)
                return
            }
            do {
                let item: Item = try JSONDecoder().decode(Item.self, from: data)
                self.delegate?.downloadedItemDetails(item.data)
            } catch {
                // log (...)
                self.delegate?.downloadedItemDetails(nil)
            }
        }
    }
    
    private func request(filename:String, completionBlock:@escaping (Data?) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let data = JSONParser.dataFromFilename(filename) {
                completionBlock(data)
            } else {
                completionBlock(nil)
            }
        }
    }
    
}
